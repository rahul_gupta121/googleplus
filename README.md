# Prerequisites #

The Google+ platform for Android has the following requirements:

*  A physical device to use for developing and testing because Google Play services can only be installed on an emulator with an AVD that runs Google APIs platform based on Android 4.2.2 or higher.

* The latest version of the Android SDK, including the SDK Tools component. The SDK is available from the Android SDK Manager.

* Your project to compile against Android 2.3 (Gingerbread) or higher.

* Eclipse configured to use Java 1.6

* The Google Play Services SDK:

   1. Launch Eclipse and select  **Window > Android SDK Manager** or run android from the command line.

   2. Scroll to the bottom of the package list and select **Extras > Google Play services.** The package is downloaded to your computer and installed in your SDK environment at <android-sdk-folder>/extras/google/google_play_services.

# Step 1: Enable the Google+ API #

1. Go to the [ Google Developers Console . ](https://console.developers.google.com/)
      
```
#!python

Note: Create a single project for the Android, iOS and web versions of your app. 
```
2. Click **Create Project**: 

    *  In the **Project Name** field, type in a name for your project. 
    *   In the **Project ID** field, optionally type in a project ID for your project or use the one that the console has created for you. This ID must be unique world-wide. 

3.Click the **Create** button and wait for the project to be created. **Note**: There may be short delay of up to 30 seconds before the project is created. Once the project is created, the name you gave it appears at the top of the left sidebar.  

4. In the left sidebar, select **APIs & auth** (the** APIs** sub-item is automatically selected).

   *  Find the ** Google+ API** and set its status to ON—notice that this action moves Google+ API to the top of the list; you can scroll up to see it.
  *  Enable any other APIs that your app requires.

5.  In the left sidebar, select** Credentials.** 

   * Click **Create a new Client ID—** the Create [Client ID dialog box appears](https://bitbucket.org/repo/jn6yy5/images/3667749312-client-id-create-android.png), as shown further below.

   * Select **Installed application** for the application type.

   * Select **Android** as the installed application type.

   * Enter your Android app's [package name .](https://developer.android.com/guide/topics/manifest/manifest-element.html#package) into the **Package name** field.

   * In a terminal, run the the [Keytool utility](https://developer.android.com/guide/publishing/app-signing.html) to get the SHA-1 fingerprint of the certificate. For the** debug.keystore**, the password is **android**. 

```
#!python

keytool -exportcert -alias androiddebugkey -keystore <path-to-debug-or-production-keystore> -list -v

Note: For Eclipse on Mac OS or Linux, the debug keystore is typically located at ~/.android/debug.keystore file path. On Windows, the debug keystore is typically located at %USERPROFILE%\.android\debug.keystore. 

```

Keytool prints the fingerprint hash to the shell. For example: 

![sha.png](https://bitbucket.org/repo/7y585A/images/2564021298-sha.png)

Copy the SHA1 fingerprint hash from your terminal. The example above highlights where to find it. 

Important: When you prepare to release your app to your users, you must follow these steps again and create a new OAuth 2.0 client ID for your production app. For production apps, you will use your own private key to sign the production app's **.apk** file. See Signing [your applications](https://developer.android.com/tools/publishing/app-signing.html) for more information. 

 *  Paste the SHA-1 fingerprint hash into the **Signing certificate fingerprint** field shown below.

 *  To activate interactive posts, enable the **Deep Linking** option. 
![clientid.png](https://bitbucket.org/repo/7y585A/images/2882693190-clientid.png)