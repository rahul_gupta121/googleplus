package developer.com.rahul.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import developer.com.rahul.R;
import developer.com.rahul.adpter.CustomFragmentPagerAdapter;
import developer.com.rahul.base.BaseActivity;
import developer.com.rahul.widget.PagerSlidingTabStrip;

public class HomeActivity extends BaseActivity /*implements SwipeRefreshLayout.OnRefreshListener*/ {

    private PagerSlidingTabStrip tabStrip;
    private ViewPager viewPager;
    private Toolbar toolBar;

    /**
     * Initializes the UI element
     */
    @Override
    public void initUI() {

        setContentView(R.layout.activity_home);

        //  swipeRefreshlayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshlayout);


        toolBar = (Toolbar) findViewById(R.id.toolBar);
        tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabStrip);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
    }

    /**
     * Initializes or creating Data element required for Activity
     */
    @Override
    public void initData() {

        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        String[] strings = getResources().getStringArray(R.array.arrayPager);
        CustomFragmentPagerAdapter fragmentPagerAdapter = new CustomFragmentPagerAdapter(getSupportFragmentManager(), strings);

        viewPager.setAdapter(fragmentPagerAdapter);
        tabStrip.setViewPager(viewPager);

       /* swipeRefreshlayout.setColorSchemeResources(android.R.color.holo_blue_light,
                                                android.R.color.holo_orange_light,
                                                android.R.color.holo_red_light,
                                                android.R.color.holo_green_light);

        swipeRefreshlayout.setOnRefreshListener(this);*/
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onRefresh() {

        refreshContent();
    }

    private void refreshContent() {

        lblTxt.setText("Refreshing....");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                swipeRefreshlayout.setRefreshing(false);
                lblTxt.setText("Refresh completed");
            }
        }, 5000);
    }*/

}
