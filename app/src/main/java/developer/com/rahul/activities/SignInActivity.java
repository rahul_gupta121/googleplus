package developer.com.rahul.activities;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import developer.com.rahul.R;
import developer.com.rahul.base.BaseActivity;
import developer.com.rahul.dialog.LoadingDialog;
import developer.com.rahul.utils.SPManager;


public class SignInActivity extends BaseActivity implements View.OnClickListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    private Button btnSignIn /*, btnNext*/;
    private String mSocialId, mSocialUsername, mSocialEmailId, mSocialProfilePicUrl, mGender;

    /** Client used to interact with Google APIs. **/
    private GoogleApiClient mGoogleApiClient;

    /** Request code used to invoke sign in user interactions. **/
    private static final int RC_SIGN_IN = 0;

    /** A flag indicating that a PendingIntent is in progress and prevents
     * us from starting further intents. **/
    private boolean mIntentInProgress;

    /** Track whether the sign-in button has been clicked so that we know to resolve
     * all issues preventing sign-in without waiting.  **/
    private boolean mSignInClicked;

    /** Store the connection result from onConnectionFailed callbacks so that we can
     * resolve them when the user clicks sign-in.  **/
    private ConnectionResult mConnectionResult;
   /* private TextView lblProfileName;
    private ImageView imgProfile;*/

    private LoadingDialog loadingDialog;
    /**
     * Initializes the UI element
     */
    @Override
    public void initUI() {

        setContentView(R.layout.activity_main);

        btnSignIn = (Button) findViewById(R.id.btnSignIn);

      //  btnNext = (Button) findViewById(R.id.btnNext);

       /* imgProfile = (ImageView) findViewById(R.id.imgProfile);

        lblProfileName = (TextView) findViewById(R.id.lblProfileName);*/
    }

    /**
     * Initializes or creating Data element required for Activity
     */
    @Override
    public void initData() {

        loadingDialog = LoadingDialog.getInstance();
        btnSignIn.setOnClickListener(this);

      //  btnNext.setOnClickListener(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

    }

    @Override
    protected void onStart() {

        super.onStart();
        mGoogleApiClient.connect();
    }


    @Override
    protected void onStop() {

        super.onStop();

        if (mGoogleApiClient.isConnected()){

            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {

        // We've resolved any connection errors.  mGoogleApiClient can be used to
        // access Google APIs on behalf of the user.
        mSignInClicked = false;

        if(mGoogleApiClient.isConnected()){

            loadingDialog.showDialog(this);
            getGooglePlusDetails();
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {

        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {

        logE("on Connection Failed");
        if (!result.hasResolution()) {

            logE("on Connection Failed get Error dialog called ");
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;
            logE("on Connection Failed connection result assigned ");

            if (mSignInClicked) {
                logE("on Connection Failed connection result assigned resolve SignIn error called ");

                // The user has already clicked 'sign-in' so we attempt to
                // resolve all errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnSignIn:

               logD("calling Google API");
               // mIntentInProgress = false;
                if (!mGoogleApiClient.isConnecting()) {
                    mSignInClicked = true;
                    resolveSignInError();
                }

                break;
           /* case R.id.btnNext:
                Intent intent = new Intent(SignInActivity.this,HomeActivity.class);
                startActivity(intent);
                break;*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	/*super.onActivityResult(requestCode, resultCode, data);*/

        switch (requestCode) {
            case RC_SIGN_IN:
                if (resultCode != RESULT_OK) {
                    mSignInClicked = false;
                }
                mIntentInProgress = false;
                if (!mGoogleApiClient.isConnecting()) {

                    mGoogleApiClient.connect();
                }
                break;

            default:

                break;
        }
    }

    /**
     A helper method to resolve the current ConnectionResult error.
     * */
    private void resolveSignInError() {

        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
                //startIntentSenderForResult(mConnectionResult.getResolution().getIntentSender(), RC_SIGN_IN, null, 0, 0, 0);
            } catch (IntentSender.SendIntentException e) {
                // The intent was canceled before it was sent.  Return to the default
                // state and attempt to connect to get an updated ConnectionResult.
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    /**
     * Fetching user's information name, email, profile pic
     * */
    private void getGooglePlusDetails() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {

                Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);

                mSocialId	= person.getId();
                mSocialUsername	= person.getDisplayName();
                mSocialEmailId = Plus.AccountApi.getAccountName(mGoogleApiClient);
                mSocialProfilePicUrl = person.getImage().getUrl().substring(0,person.getImage().getUrl().length() - 2) + 500/*Image Size*/;
                switch (person.getGender()) {

                    case Person.Gender.MALE:
                        mGender = "MALE";
                        break;
                    case Person.Gender.FEMALE:
                        mGender = "FEMALE";
                        break;
                    case Person.Gender.OTHER:
                        mGender = "OTHER";
                        break;
                }


              /*  Picasso.with(SignInActivity.this).load(mSocialProfilePicUrl).into(imgProfile);
                lblProfileName.setText("Name "+mSocialUsername+"\nEmail ID "+mSocialEmailId);*/

                logD("Social Id == "+mSocialId);
                logD("Profile_UserName == "+mSocialUsername);
                logD("Profile_Image == "+mSocialProfilePicUrl);
                logD("Gender "+ mGender);
                SPManager.save(SPManager.KEY_USERID, mSocialId);
                SPManager.save(SPManager.KEY_USERNAME,mSocialUsername);
                SPManager.save(SPManager.KEY_PROFILE_PIC,mSocialProfilePicUrl);
                SPManager.save(SPManager.KEY_GENDER,mGender);
                SPManager.saveBoolean(SPManager.KEY_LOGGED_IN, true);

                launchHomeActivity();
                /***get the google plus details and do signout. no more need of google plus session***/

            //if()
               signoutGoogle();
            } else {

                signoutGoogle();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }finally {
           loadingDialog.dismissDialog();
        }
    }

    private void launchHomeActivity() {

        finish();
        //push from top to bottom
        //overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
        //slide from right to left

        Intent intent = new Intent(this,HomeActivity.class);

        startActivity(intent);
    }

    private void signoutGoogle() {

        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }
}
