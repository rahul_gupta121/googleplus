package developer.com.rahul.activities;

import android.content.Intent;

import java.util.Timer;
import java.util.TimerTask;

import developer.com.rahul.R;
import developer.com.rahul.base.BaseActivity;
import developer.com.rahul.utils.SPManager;

public class SplashActivity extends BaseActivity {

    private int Delay = 3000;

    /**
     * Initializes the UI element
     */
    @Override
    public void initUI() {

        setContentView(R.layout.activity_splash);
    }

    /**
     * Initializes or creating Data element required for Activity
     */
    @Override
    public void initData() {


    }

    @Override
    protected void onResume() {
        super.onResume();

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                finish();
                //push from top to bottom
                //overridePendingTransition(R.anim.push_down_in, R.anim.push_down_out);
                //slide from right to left
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);

                //slide from left to right
                // overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                timer.cancel();

                launchActivity();


            }
        }, Delay);
    }

    private void launchActivity() {

        Intent intent = null;

        if (!SPManager.retriveBoolean(SPManager.KEY_LOGGED_IN)) {

            intent = new Intent(SplashActivity.this, SignInActivity.class);
        } else {

            intent = new Intent(SplashActivity.this, HomeActivity.class);
        }

        startActivity(intent);
    }
}