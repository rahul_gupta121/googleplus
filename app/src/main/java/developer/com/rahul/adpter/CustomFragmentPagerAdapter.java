package developer.com.rahul.adpter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;
import android.view.ViewGroup;

import developer.com.rahul.fragments.FragmentFriendList;
import developer.com.rahul.fragments.FragmentUserDetail;

/**
 *
 * Created by rahul on 6/5/15.
 */
public class CustomFragmentPagerAdapter extends FragmentPagerAdapter {

    private String TAG = "CustomFragmentPagerAdapter";
    private String [] strArr;
    private FragmentManager fragmentManager;
    public CustomFragmentPagerAdapter(FragmentManager fm,String[] strArr) {

        super(fm);

        this.strArr = strArr;
        this.fragmentManager = fm;
    }

    /**
     * Return the Fragment associated with a specified position.
     *
     * @param position
     */
    @Override
    public Fragment getItem(int position) {

        Log.e(TAG,"Get item get called ...");
        switch (position) {

            case 0:
              return   FragmentUserDetail.newInstance();

            case 1:

          return FragmentFriendList.newInstance("", "FriendList");
        }
        return null;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        Log.e(TAG,"instantiateItem get called ...");

        return super.instantiateItem(container, position);
    }


    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {

        return strArr.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return strArr[position].toUpperCase();
    }
}
