package developer.com.rahul.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import developer.com.rahul.R;
import developer.com.rahul.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentFriendList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentFriendList extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentFriendList.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentFriendList newInstance(String param1, String param2) {

        Log.d(TAG,"newInstance ...  FragmentFriendList");

        FragmentFriendList fragment = new FragmentFriendList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentFriendList() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            logD("onCreate ...  FragmentFriendList");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        view = inflater.inflate(R.layout.fragment_friend_list, container, false);
        logD("onCreateView ...  FragmentFriendList");

        initUI(view);

        return view;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        logD("onActivityCreated ...  FragmentFriendList");

        initData();
    }

    /**
     * Initializes the UI element
     *
     * @param view
     */
    @Override
    protected void initUI(View view) {

    }

    /**
     * Initializes or creating Data element required for Activity
     */
    @Override
    protected void initData() {

    }
}