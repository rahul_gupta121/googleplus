package developer.com.rahul.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import developer.com.rahul.R;
import developer.com.rahul.base.BaseFragment;
import developer.com.rahul.utils.SPManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FragmentUserDetail#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentUserDetail extends BaseFragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private View view;
    private TextView lblProfileName,lblGender;
    private ImageView imgProfile;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragmentUserDetail.
     */
    public static FragmentUserDetail newInstance() {

        FragmentUserDetail fragment = new FragmentUserDetail();
        return fragment;
    }

    public FragmentUserDetail() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_user_detail, container, false);

        initUI(view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        initData();
    }

    /**
     * Initializes the UI element
     *
     * @param view
     */
    @Override
    protected void initUI(View view) {

        imgProfile = (ImageView) view.findViewById(R.id.imgProfile);
        lblProfileName = (TextView) view.findViewById(R.id.lblProfileName);
        lblGender = (TextView) view.findViewById(R.id.lblGender);

    }

    /**
     * Initializes or creating Data element required for Activity
     */
    @Override
    protected void initData() {

        Picasso.with(getActivity()).load(SPManager.retrive(SPManager.KEY_PROFILE_PIC)).into(imgProfile);
        lblProfileName.setText(SPManager.retrive(SPManager.KEY_USERNAME));
        lblGender.setText(SPManager.retrive(SPManager.KEY_GENDER));
    }
}